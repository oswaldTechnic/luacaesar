#!/bin/lua
--[=[	Basic ciphers to understand String manipulation, Lua syntax, and the NeoVim editor.
--]=]

LOWERCASE = "abcdefghijklmnopqrstuvwxyz"
UPPERCASE = string.upper(LOWERCASE)

function main()
	local inputText = "The quick brown fox jumps over the lazy dog. 1234567890?!"
	print("Shifter: " .. shiftChar(1, "2"))
	print("Caesar: " .. caesar(0, inputText))
	print("Vigenere: " .. vigenere("beehive", inputText))
end

function shiftChar(shift, character)
	if character == ("\46") then return("\46") end
	local output = character
	while shift < 0 do shift = shift + 26 end
	shift = shift % 26
	local guess = string.find(LOWERCASE, character)
	if guess ~= nil then
		output = string.sub(LOWERCASE, (guess + shift - 1) % 26 + 1, (guess + shift - 1) % 26 + 1)
	else
		local guess = string.find(UPPERCASE, character)
		if guess ~= nil then
			output = string.sub(UPPERCASE, (guess + shift - 1) % 26 + 1, (guess + shift - 1) % 26 + 1)
		end
	end
	return(output)
end

function caesar(shift, text)
	local output = ""
	for char in text:gmatch "." do
		output = output .. shiftChar(shift, char)
	end
	return output
end

function vigenere(key, text)
	local e, i, keyList, output = 0, 1, {}, ""
	for char in key:gmatch "." do
		e = e + 1
		keyList[e] = string.find(LOWERCASE, char)
		if keyList[e] == nil then
			keyList[e] = string.find(UPPERCASE, char)
			if keyList[e] == nil then
				keyList[e] = 0
			end
		end
	end

	for char in text:gmatch "." do
		output = output .. shiftChar(keyList[((i-1)%e)+1] , char)
	end
	return (output)
end

main()
